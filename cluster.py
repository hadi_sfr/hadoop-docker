#!/usr/bin/python


from itertools import chain

from mininet.net import Containernet
from mininet.node import Controller
from mininet.cli import CLI
from mininet.link import TCLink
from mininet.log import info, setLogLevel


def colorize(string):
    return '\n\033[36m' + string + '\033[39m\n'


setLogLevel('info')

net = Containernet(controller=Controller)

info(colorize('*** Adding controller'))
net.addController('c0')

core_name = 's0'
access_pure_names = [str(i + 1) for i in xrange(2)]
hosts_names = {'s' + access: ['d' + access + str(i + 1) for i in range(2)] for access in access_pure_names}
access_names = hosts_names.keys()
master = None
ports = [50070, 50075, 8088, 8888]

info(colorize('*** Adding switches'))
core = net.addSwitch('s0')
access = {name: net.addSwitch(name) for name in access_names}

info(colorize('*** Adding docker containers'))
ip_counter = 30
ips = dict()
for access_to in hosts_names:
    for host in hosts_names[access_to]:
        ip_counter += 1
        ips[host] = '.'.join(['10.0.0', str(ip_counter)])
hosts = {access_to: [net.addDocker(host, ip=ips[host], dimage='cnp2', **(
    [{}, {
        "port_bindings": {port: port for port in ports},
        "ports": ports,
        "publish_all_ports": True
    }][access_to == access_names[0] and host == hosts_names[access_names[0]][0]])
) for host in hosts_names[access_to]] for access_to in access}
all_hosts = list(chain(*hosts.values()))

info(colorize('*** Creating links'))
for node in access.values():
    link = net.addLink(core, node, cls=TCLink, delay='5ms', bw=1)
for (name, node) in access.items():
    for host in hosts[name]:
        link = net.addLink(node, host, cls=TCLink, delay='10ms', bw=5)

info(colorize('*** Starting network'))
net.start()
info(colorize('*** Testing connectivity'))
net.ping(all_hosts)

info(colorize('*** Setup nodes'))
master = {'node': hosts[access_names[0]][0], 'ip': ips[hosts_names[access_names[0]][0]]}
info(colorize('master: %s' % str(master)))
ips_str = []
ip_counter = 0
for (name, ip) in ips.items():
    ip_counter += [1, 0][ip == master['ip']]
    ips_str.append(' '.join([ip, ['slave' + str(ip_counter), 'master'][ip == master['ip']]]))
for host in all_hosts:
    host.cmd('/usr/sbin/sshd -D &')  # start ssh on hosts
    host.cmd('export HADOOP_HOSTS=\"' + ','.join(ips_str) + '\"')
    if host is master['node']:
        host.cmd('export MY_ROLE="master"')
    else:
        host.cmd('/$HADOOP_HOME/etc/hadoop/start.sh > ~/result')
master['node'].cmd('/$HADOOP_HOME/etc/hadoop/start.sh > ~/result')

info(colorize('*** Running CLI'))
CLI(net)
info(colorize('*** Stopping network'))
net.stop()
