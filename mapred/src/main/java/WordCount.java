import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

public class WordCount {

    private static final Path hdfsStopWordsPath = new Path("/user/sina/stop-words.keys");
    private static final String localStopWordsPath = "/home/stop-words.keys";
    private static final Path hdfsRawDataPath = new Path("/user/sina/raw.data");
    private static final String localRawDataPath = "/home/raw.data";
    private static final String hdfsOutputPath = "/user/sina/output/part-r-0000";
    private static final String localOutputPath = "/home/output/part";
    private static final int reducersNum = 10;

    private static class Keyword {

        private static final String keywordsPath = "/home/wikipedia.keys";
        private static final String dataFilePath = "wikipedia.data";
        private static final String wikipediaUrl = "https://en.wikipedia.org/wiki/<keyword>?action=raw";
        private static final String startOfAdditionalData = "==References==";

        private ArrayList<String> keywords;

        public Keyword() {
            getKeywords();
            for (String keyword : keywords) {
                if (addDatatoFile(removeAdditionalData(getDataFromServer(keyword))))
                    break;
            }
            addToRawFile();
        }

        private void getKeywords() {
            keywords = new ArrayList<String>();
            try {
                File file = new File(keywordsPath);
                BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
                String line;
                while ((line = bufferedReader.readLine()) != null)
                    keywords.add(line);
            } catch (java.io.IOException e) {
                e.printStackTrace();
            }
        }

        private String getDataFromServer(String keyword) {
            OkHttpClient client = new OkHttpClient();
            String url = wikipediaUrl.replace("<keyword>", keyword);
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            try {
                Response response = client.newCall(request).execute();
                return response.body().string();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        private boolean addDatatoFile(String data) {
            try {
                BufferedWriter writer = new BufferedWriter(new FileWriter(dataFilePath, true));
                writer.append(data);
                writer.close();
                File file = new File(dataFilePath);
                return file.length() > 10000000;
            } catch (IOException e) {
                e.printStackTrace();
            }
            return true;
        }

        private String removeAdditionalData(String input) {
            int additionalIndex = input.indexOf(startOfAdditionalData);
            if (additionalIndex == -1)
                return input;
            return input.substring(0, additionalIndex);
        }

        private void addToRawFile() {
            for (int i = 0; i < 26; i++) {
                try {
                    BufferedReader bufferedReader = new BufferedReader(new FileReader(new File(dataFilePath)));
                    BufferedWriter writer = new BufferedWriter(new FileWriter(localRawDataPath, true));
                    String line;
                    while ((line = bufferedReader.readLine()) != null)
                        writer.append(line);
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static void collectData() {
        Keyword keyword = new Keyword();
        keyword = null;
    }

    public static class HdfsWriter extends Configured implements Tool {

        static final String FS_PARAM_NAME = "fs.defaultFS";

        @Override
        public int run(String[] args) throws Exception {
            if (args.length < 2) {
                System.err.println("HdfsWriter [local input path] [hdfs output path]");
                return 1;
            }

            String localInputPath = args[0];
            Path outputPath = new Path(args[1]);

            Configuration conf = getConf();
            System.out.println("configured filesystem = " + conf.get(FS_PARAM_NAME));
            FileSystem fs = FileSystem.get(conf);
            if (fs.exists(outputPath)) {
                System.err.println("output path exists");
                return 1;
            }
            OutputStream os = fs.create(outputPath);
            InputStream is = new BufferedInputStream(new FileInputStream(localInputPath));
            IOUtils.copyBytes(is, os, conf);
            return 0;
        }
    }

    public static class HdfsReader extends Configured implements Tool {

        public static final String FS_PARAM_NAME = "fs.defaultFS";

        public int run(String[] args) throws Exception {

            if (args.length < 2) {
                System.err.println("HdfsReader [hdfs input path] [local output path]");
                return 1;
            }

            Path inputPath = new Path(args[0]);
            String localOutputPath = args[1];
            Configuration conf = getConf();
            System.out.println("configured filesystem = " + conf.get(FS_PARAM_NAME));
            FileSystem fs = FileSystem.get(conf);
            InputStream is = fs.open(inputPath);
            OutputStream os = new BufferedOutputStream(new FileOutputStream(localOutputPath));
            IOUtils.copyBytes(is, os, conf);
            return 0;
        }
    }


    public static class TokenizerMapper
            extends Mapper<Object, Text, Text, IntWritable> {

        private final static IntWritable one = new IntWritable(1);
        private Text word = new Text();
        private HashSet<String> stopWords = new HashSet<>();

        private static final Log LOG = LogFactory.getLog(TokenizerMapper.class);

        private boolean isLetters(String name) {
            char[] chars = name.toCharArray();

            for (char c : chars) {
                if (!Character.isLetter(c)) {
                    return false;
                }
            }

            return true;
        }

        private void loadStopWords(Context context) {
            try {
                FileSystem fs = FileSystem.get(context.getConfiguration());
                BufferedReader bufferedReader =
                        new BufferedReader(new InputStreamReader(fs.open(hdfsStopWordsPath)));
                String line;
                while ((line = bufferedReader.readLine()) != null)
                    stopWords.add(line);
                bufferedReader.close();
            } catch (java.io.IOException e) {
                LOG.info("Can't open stop-words.keys");
            }
        }

        @Override
        protected void map(Object key, Text value, Context context) throws IOException, InterruptedException {
            StringTokenizer itr = new StringTokenizer(value.toString());
            while (itr.hasMoreTokens()) {
                String currentToken = itr.nextToken();
                if (stopWords.contains(currentToken) || !isLetters(currentToken)) {
                    continue;
                }
                word.set(currentToken.toLowerCase());
                context.write(word, one);
            }
        }

        @Override
        protected void setup(Context context) throws IOException, InterruptedException {
            try {
                ToolRunner.run(new HdfsWriter(), new String[]{localStopWordsPath, hdfsStopWordsPath.toString()});
            } catch (Exception e) {
                e.printStackTrace();
            }
            loadStopWords(context);
        }
    }

    public static class IntSumReducer
            extends Reducer<Text, IntWritable, Text, IntWritable> {

        private static final Log LOG = LogFactory.getLog(IntSumReducer.class);

        private HashMap<Text, Integer> topValues = new HashMap<Text, Integer>();

        private static final List<Integer> numbersOfTopValues = Arrays.asList(2, 3);
        private static final int maxNumberOfTopValues = Collections.max(numbersOfTopValues);

        @Override
        protected void reduce(Text key, Iterable<IntWritable> values, Context context)
                throws IOException, InterruptedException {
            int sum = 0;
            for (IntWritable val : values) {
                sum += val.get();
            }

            topValues.put(new Text(key), sum + topValues.getOrDefault(key, 0));
        }

        @Override
        protected void cleanup(Context context) throws IOException, InterruptedException {
            Map<Text, Integer> topValuesSorted = topValues.entrySet().stream()
                    .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder())).collect(Collectors.toMap(
                            Map.Entry::getKey,
                            Map.Entry::getValue,
                            (e1, e2) -> e1,
                            LinkedHashMap::new
                    ));
            int index = 0;
            for (Text key : topValuesSorted.keySet()) {
                if (index++ >= maxNumberOfTopValues)
                    break;
                context.write(key, new IntWritable(topValues.get(key)));
            }
        }
    }

    public static void main(String[] args) throws Exception {
        collectData();
        ToolRunner.run(new HdfsWriter(), new String[]{localRawDataPath, hdfsRawDataPath.toString()});
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf, "WordCount");
        job.setJarByClass(WordCount.class);
        job.setMapperClass(TokenizerMapper.class);
        job.setCombinerClass(IntSumReducer.class);
        job.setReducerClass(IntSumReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);
        job.setNumReduceTasks(reducersNum);
        FileInputFormat.addInputPath(job, hdfsRawDataPath);
        FileOutputFormat.setOutputPath(job, new Path(args[0]));
        boolean returncode = job.waitForCompletion(true);
        if (returncode) {
            for (int i = 0; i < reducersNum; i++)
                ToolRunner.run(new HdfsReader(), new String[]{hdfsOutputPath + i, localOutputPath + i});
        }
        System.exit(returncode ? 0 : 1);
    }
}
