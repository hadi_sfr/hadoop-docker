#!/usr/bin/env bash

jar_name="map-red-1.0-SNAPSHOT.jar"
class_name="WordCount"
run_params="/user/sina/output"

NC="\e[0m"
CODE="\e[90m"

if [[ $# != 1 ]]; then
    echo -e "Usage:\t${CODE}${0} <docker_name>${NC}\nYou can obtain ${CODE}<docker_name>${NC} with \`${CODE}docker ps${NC}\`:"
    docker ps --format "table {{.Names}}\t{{.ID}}\t{{.Image}}"
    exit 1
fi

mvn package
docker cp target/${jar_name} ${1}:/home
docker cp ../stop-words.keys ${1}:/home/stop-words.keys
# docker cp ../wikipedia.data ${1}:/home/wikipedia.data
docker cp ../wikipedia.keys ${1}:/home/wikipedia.keys
echo -e "\
#!/usr/bin/env bash
hdfs dfs -rm -r /user/sina/ &>/dev/null  # clean up
hdfs dfs -mkdir -p /user/sina/
rm -r /home/output/ &>/dev/null
mkdir /home/output/
hadoop jar ${jar_name} ${class_name} ${run_params}
" > run.sh
chmod +x run.sh
docker cp run.sh ${1}:/home
echo -e "Run \`${CODE}run.sh${NC}\` in '${CODE}${1}:/home${NC}'."
echo -e "You may need to run '${CODE}\$HADOOP_HOME/etc/hadoop/start.sh${NC}' and '${CODE}source ~/.bashrc${NC}' on docker before."
rm run.sh
