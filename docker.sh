#!/usr/bin/env bash

docker run -p 8088:8088 -p 50070:50070 -p 50075:50075 --entrypoint sh -it cnp2 -c '$HADOOP_HOME/etc/hadoop/start.sh && /bin/bash'
